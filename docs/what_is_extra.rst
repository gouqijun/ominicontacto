.. _about_webrtc:

WebRTC - Tecnología subyacente de OMniLeads
*********************************************
Antes de de citar los casos de uso, ponemos énfasis en repasar los beneficios de la tecnología WebRTC; núcleo de OMniLeads.

WebRTC dota a un navegador web de la posibilidad de mantener comunicaciones real time de voz, video, chat y compartición de pantalla.
OMniLeads se nutre de esta tecnología para nuclear las comunicaciones y la interfaz de gestión web, evitando el uso de aplicaciones de escritorio
"softphones", lo cual otorga una inmediatez en términos de "click and work" en el alta usuarios ya que a partir de un login web, están en línea
procesando comunicaciones.


.. image:: images/what_is_webrtc.png
        :align: center

**Otras ventajas de WebRTC**

- Se minimizan los punto de fallo en las esrtaciones de trabajo
- Se minimizan las tareas del helpdesk y personal de soporte
- Trabaja con el codec de audio Opus y video VP8, ambos concebidos para la performance en real-time & cloud
- En términos de seguridad todas las comunicaciones viajan cifradas en términos de señalización y media


.. _about_omlfeatures:

Características y funcionalidades de OMniLeads
**********************************************
- Gestión de campañas Entrantes y Salientes; Discador predictivo, Preview & IVR Blaster.
- Consola de agente WebRTC (no se requiere instalar ninguna aplicación ni plugin, 100% Browser).
- Opus ® como codec por defecto para todas las comunicaciones de Agente.
- Consola de supervisión WebRTC; detalle de estados de agentes y campañas.
- Reportes de productividad de agentes y campañas.
- Búsqueda de grabaciones con filtros de campaña, agente, llamadas "observadas", etc.
- Reciclado de bases por calificación de agente y/o status telefónico.
- Cambio de base de contactos sobre la misma campaña.
- Detección de contestadores con reproducción de mensaje de audio.
- Listo para virtualizar ! OML fue concebido como una tecnología orientada a los entornos de virtualización.
- Listo para escalar ! La escalabilidad es posible debido al hecho utilizar tecnologías subyacentes muy potentes como Postgres, Nginx, Rtpengine, que a su vez pueden correr en hosts independientes (escalabilidad horizontal) con una mínima configuración.
- Addons complementarios que dotan a la plataforma de funcionalidades extras y/o para segmentos verticales.
- 100% orientado a Contact Center. No se trata de un software de PBX con agregados de reportería y/o supervisión. OML fue concebida desde cero, como una plataforma orientada y optimizada para el Contact Center.

Release Notes
*************

*Octubre 31, 2019*

Detalles de Release 1.3.3
=========================

Admin de OML
-------------
- Se soluciona error que impedía adicionar parámetros de interacción CRM en la gestión de las campañas
